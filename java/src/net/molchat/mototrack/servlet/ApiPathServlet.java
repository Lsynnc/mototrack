package net.molchat.mototrack.servlet;

import net.molchat.mototrack.constants.ParamConst;
import net.molchat.mototrack.constants.SessionConst;
import net.molchat.mototrack.db.entity.GeoPointData;
import net.molchat.mototrack.error.ErrorData;
import net.molchat.mototrack.error.ErrorType;
import net.molchat.mototrack.facade.PointFacade;
import net.molchat.mototrack.facade.PointFacadeImpl;
import net.molchat.mototrack.settings.Settings;
import net.molchat.mototrack.util.JsonUtil;
import net.molchat.mototrack.util.JsonUtilGsonImpl;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.logging.Logger;

/**
 * @author Valentyn Markovych, Gorilla
 */
public class ApiPathServlet extends HttpServlet {

    private final Logger LOG = Logger.getLogger(ApiPathServlet.class.getName());

    private PointFacade pointFacade;
    private JsonUtil jsonUtil;
    private Settings settings = Settings.getInstance();


    @Override
    public void init(ServletConfig config) throws ServletException {

        super.init(config);
        pointFacade = new PointFacadeImpl();
        jsonUtil = new JsonUtilGsonImpl();
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        logRequest(req);


        Object result = null;

        String command = req.getParameter("c");
        if (command == null || command.isEmpty()) {
            result = pointFacade.getRandomGeoPath("random", 50);
        } else {
            command = command.toLowerCase();
            switch (command) {
                case "get":
                    result = getPath(req);
                    break;
                case "add":
                    result = addPoint(req);
                    break;
                case "random":
                    result = pointFacade.getRandomGeoPath("random", 50);
                    break;
                default:
                    result = "Unknown command";
            }
        }

        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");

        PrintWriter writer = resp.getWriter();
        writer.print(jsonUtil.toJson(result));
        writer.close();
    }


    /**
     * Log the request (parameters, session id, etc.)
     *
     * @param req
     */
    private void logRequest(HttpServletRequest req) {

        // Common request characteristics
        String contextPath = req.getContextPath();
        String method = req.getMethod();
        String requestURI = req.getRequestURI();
        StringBuffer requestURL = req.getRequestURL();

        // Session ID
        HttpSession session = req.getSession(false);
        String sessionId;
        if (session != null) {
            sessionId = session.getId();
        } else {
            sessionId = null;
        }

        StringBuilder requestBuf = new StringBuilder();

        requestBuf.append(method);
        requestBuf.append(" ").append(requestURL);
        requestBuf.append("\nsessionId: ").append(sessionId).append('\n');

        // Log parameters map
        Map<String, String[]> parameterMap = req.getParameterMap();
        Set<Map.Entry<String, String[]>> entries = parameterMap.entrySet();
        for (Map.Entry param : entries){
            requestBuf.append(param.getKey()).append(": ");
            String[] values = (String[]) param.getValue();
            for (String value : values){
                requestBuf.append(value).append(", ");
            }
            requestBuf.append('\n');
        }

        LOG.info(requestBuf.toString());



    }

    private Object addPoint(HttpServletRequest req) {

        String markerId = req.getParameter(ParamConst.MARKER_ID);
        if (markerId == null || markerId.isEmpty()) {
            return new ErrorData(ErrorType.NO_MARKER_ID_PASSED);
        }

        GeoPointData point = new GeoPointData();
        try {
            String latitude = req.getParameter(ParamConst.LATITUDE);
            String longitude = req.getParameter(ParamConst.LONGITUDE);

            point.setX(new BigDecimal(latitude).setScale(8, BigDecimal.ROUND_HALF_UP));
            point.setY(new BigDecimal(longitude).setScale(8, BigDecimal.ROUND_HALF_UP));

            // Timestamp
            point.setTimestamp(System.currentTimeMillis());
            String timeStr = req.getParameter(ParamConst.TIME);
            if (timeStr != null) {
                HttpSession session = req.getSession(true);
                String timeParamFormatStr = (String) session.getAttribute(SessionConst.TIME_PARAM_FORMAT);
                if (timeParamFormatStr == null) {
                    timeParamFormatStr = settings.getDefaultDateTimeParamFormat();
                    session.setAttribute(SessionConst.TIME_PARAM_FORMAT, timeParamFormatStr);
                }

                try {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(timeParamFormatStr);
                    simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
                    point.setTimestamp(simpleDateFormat.parse(timeStr).getTime());
                } catch (Exception e) {
                    // Just leave system timestamp
                    LOG.fine("Error during parsing '" + ParamConst.TIME + '=' + timeStr + "' parameter as '" + timeParamFormatStr + '\'');
                }
            } else {
                timeStr = req.getParameter(ParamConst.TIMESTAMP);
                if (timeStr != null) {
                    try {
                        point.setTimestamp(Long.parseLong(timeStr));
                    } catch (Exception e) {
                        // Just leave system timestamp
                        LOG.fine("Error during parsing '" + ParamConst.TIMESTAMP + '=' + timeStr + "' parameter as Long");
                    }
                }
            }

            // Timestamp

        } catch (Exception e) {
            point = null;
        }

        if (point != null) {
            pointFacade.addPoint(markerId, point);
            return new ErrorData(ErrorType.OK);
        }

        return new ErrorData(ErrorType.UNKNOWN);
    }

    private Object getPath(HttpServletRequest req) {

        String markerId = req.getParameter(ParamConst.MARKER_ID);
        if (markerId == null || markerId.isEmpty()) {
            return new ErrorData(ErrorType.NO_MARKER_ID_PASSED);
        }

        String maxPointsString = req.getParameter(ParamConst.MAX_POINTS);
        int maxPoints;
        try {
            maxPoints = Integer.parseInt(maxPointsString);
        } catch (Exception e) {
            maxPoints = settings.getDefaultPointsNum();
        }

        String maxMinutesString = req.getParameter(ParamConst.MAX_MINUTES);
        int maxMinutes;
        try {
            maxMinutes = Integer.parseInt(maxMinutesString);
        } catch (Exception e) {
            maxMinutes = settings.getDefaultMaxMinutes();
        }

        return pointFacade.getPath(markerId, maxMinutes, maxPoints);
    }

}
