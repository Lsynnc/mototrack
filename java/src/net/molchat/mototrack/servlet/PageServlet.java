package net.molchat.mototrack.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Valentyn Markovych, Gorilla
 */
public class PageServlet extends HttpServlet {

    public final static String PAGE_VIEW_PREFIX = "/WEB-INF/view/";
    public final static String PAGE_VIEW_SUFFIX = ".jsp";
    public final static String DEFAULT_PAGE = ".jsp";


    protected String getViewForName(String viewName) {

        return PAGE_VIEW_PREFIX + viewName + PAGE_VIEW_SUFFIX;
    }


    protected void renderView(HttpServletRequest request, HttpServletResponse response, String viewName) throws ServletException, IOException {

        request.getRequestDispatcher(getViewForName(viewName)).forward(request, response);
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        renderView(request, response, DEFAULT_PAGE);
    }
}
