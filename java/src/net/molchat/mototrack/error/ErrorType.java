package net.molchat.mototrack.error;

/**
 * @author Valentyn Markovych, Gorilla
 */
public enum ErrorType {

    NO_MARKER_ID_PASSED(100), OK(0), UNKNOWN(1);

    private int errorNumber;

    ErrorType(int errorNumber) {
        this.errorNumber = errorNumber;
    }

    public int getErrorNumber() {

        return errorNumber;
    }
}
