package net.molchat.mototrack.error;

/**
 * @author Valentyn Markovych, Gorilla
 */
public class ErrorData {

    private int errorNum;


    public ErrorData(ErrorType errorType) {

        this.errorNum = errorType.getErrorNumber();
    }
}
