package net.molchat.mototrack.constants;

/**
 * @author Valentyn Markovych, Gorilla
 */
public class ParamConst {
    public static final String MARKER_ID = "id";
    public static final String LATITUDE = "lat";
    public static final String LONGITUDE = "lon";
    public static final String MAX_POINTS = "maxPoints";
    public static final String MAX_MINUTES = "maxMinutes";
    public static final String TIME = "time";
    public static final String TIMESTAMP = "timeStamp";
}
