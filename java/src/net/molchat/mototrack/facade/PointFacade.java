package net.molchat.mototrack.facade;

import net.molchat.mototrack.db.entity.GeoPathData;
import net.molchat.mototrack.db.entity.GeoPointData;

import java.util.List;

/**
 * @author Valentyn Markovych, Gorilla
 */
public interface PointFacade {

    public GeoPathData getRandomGeoPath(String markerId, int maxPoints);

    public GeoPathData getPath(String markerId, int maxMinutes, int maxPoints);

    public void addPoint(String markerId, GeoPointData point);
}
