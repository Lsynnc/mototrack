package net.molchat.mototrack.facade;

import net.molchat.mototrack.db.DbManager;
import net.molchat.mototrack.db.DbService;
import net.molchat.mototrack.db.entity.GeoPathData;
import net.molchat.mototrack.db.entity.GeoPointData;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Valentyn Markovych, Gorilla
 */
public class PointFacadeImpl implements PointFacade {

    private DbService dbService = DbManager.getInstance().getDbService();

    private GeoPointData getRandomGeoPoint() {

        double startX = 50.48;
        double startY = 30.55;

        double deltaX = 0.01d;
        double deltaY = 0.01d;

        double currentX = startX + Math.random() * deltaX - Math.random() * deltaX;
        double currentY = startY + Math.random() * deltaY - Math.random() * deltaY;

        GeoPointData geoPointData = new GeoPointData();
        geoPointData.setTimestamp(System.currentTimeMillis());
        geoPointData.setX(BigDecimal.valueOf(currentX).setScale(8, RoundingMode.HALF_UP));
        geoPointData.setY(BigDecimal.valueOf(currentY).setScale(8, RoundingMode.HALF_UP));
        return geoPointData;
    }

    @Override
    public GeoPathData getRandomGeoPath(String markerId, int maxPoints) {

        GeoPathData geoPathData = new GeoPathData();
        geoPathData.setMarkerId(markerId);

        int pointsNum = maxPoints;
        List<GeoPointData> geoPointDatas = new ArrayList<GeoPointData>();

        for (int q = pointsNum - 1; q >= 0; --q) {
            geoPointDatas.add(getRandomGeoPoint());
        }

        geoPathData.setGeoPoints(geoPointDatas);
        return geoPathData;
    }


    @Override
    public GeoPathData getPath(String markerId, int maxMinutes, int maxPoints) {

        GeoPathData geoPathData = new GeoPathData();
        geoPathData.setMarkerId(markerId);

        geoPathData.setGeoPoints(dbService.getPath(markerId, maxMinutes, maxPoints));
        return geoPathData;
    }

    @Override
    public void addPoint(String markerId, GeoPointData point) {

        dbService.addPoint(markerId, point);
    }
}
