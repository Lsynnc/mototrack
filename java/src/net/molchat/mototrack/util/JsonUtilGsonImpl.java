package net.molchat.mototrack.util;

import com.google.gson.Gson;

/**
 * @author Valentyn Markovych, Gorilla
 */
public class JsonUtilGsonImpl implements JsonUtil {


    @Override
    public String toJson(Object dataObject) {

        Gson gson = new Gson();
        return gson.toJson(dataObject);
    }
}
