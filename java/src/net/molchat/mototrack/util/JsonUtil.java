package net.molchat.mototrack.util;

/**
 * @author Valentyn Markovych, Gorilla
 */
public interface JsonUtil {

    public String toJson(Object object);
}
