package net.molchat.mototrack.db;

import net.molchat.mototrack.db.entity.GeoPointData;

import java.util.List;

/**
 * @author Valentyn Markovych, Gorilla
 */
public interface DbService {

    public void addPoint(String markerId, GeoPointData geoPointData);

    /**
     * Returns path for requested marker. Limited by both maximum of points and maximum of minutes, whichever limit comes first.
     *
     * @param markerId ID for marker path is getting for
     * @param maxMinutes    minutes limit
     * @param maxPoints     points limit
     * @return Path points for requested marker
     */
    public List<GeoPointData> getPath(String markerId, int maxMinutes, int maxPoints);
}
