package net.molchat.mototrack.db;

import net.molchat.mototrack.db.entity.GeoPointData;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Valentyn Markovych, Gorilla
 */
public class InMemoryDbService implements DbService {

    // Database
    private Map<String, List<GeoPointData>> db = new HashMap<>();


    public InMemoryDbService() {
    }


    @Override
    public void addPoint(String markerId, GeoPointData geoPointData) {

        // TODO add checking total length here

        // Get the previous data
        synchronized (db) {
            List<GeoPointData> geoPointDatas = db.get(markerId);

            if (geoPointDatas == null) {
                geoPointDatas = new LinkedList<>();
                db.put(markerId, geoPointDatas);
            }

            geoPointDatas.add(geoPointData);
        }
    }

    @Override
    public List<GeoPointData> getPath(String markerId, int maxMinutes, int maxPoints) {

        LinkedList<GeoPointData> geoPointDatasResult;
        long fromTime = System.currentTimeMillis() - maxMinutes * 1000 * 60;

        synchronized (db) {
            LinkedList<GeoPointData> geoPointDatasDb = (LinkedList<GeoPointData>) db.get(markerId);
            if (geoPointDatasDb == null || geoPointDatasDb.size() <= 0) {
                geoPointDatasResult = null;
            } else {
                geoPointDatasResult = new LinkedList<>();
                Iterator<GeoPointData> geoPointDataIterator = geoPointDatasDb.descendingIterator();

                int added = 0;
                while (added < maxPoints && geoPointDataIterator.hasNext()) {
                    GeoPointData point = geoPointDataIterator.next();
                    if (maxMinutes <= 0 || point.getTimestamp() >= fromTime) {
                        geoPointDatasResult.addFirst(point);
                        added++;
                    } else {
                        break;
                    }
                }
            }
        }

        return geoPointDatasResult;
    }
}
