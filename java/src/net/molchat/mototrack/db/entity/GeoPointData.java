package net.molchat.mototrack.db.entity;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Valentyn Markovych, Gorilla
 */
public class GeoPointData {

    protected BigDecimal x; // Longitude
    protected BigDecimal y; // Latitude
    protected long timestamp; // Date, in GMT+0

    public GeoPointData() {
    }

    public BigDecimal getX() {
        return x;
    }

    public void setX(BigDecimal x) {
        this.x = x;
    }

    public BigDecimal getY() {
        return y;
    }

    public void setY(BigDecimal y) {
        this.y = y;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "GeoPointData{" +
                "timestamp=" + timestamp +
                ", x=" + x +
                ", y=" + y +
                '}';
    }
}
