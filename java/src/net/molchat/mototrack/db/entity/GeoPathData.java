package net.molchat.mototrack.db.entity;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author Valentyn Markovych, Gorilla
 */
public class GeoPathData {

    private String markerId;
    private List<GeoPointData> geoPoints;

    public GeoPathData() {
    }

    public String getMarkerId() {
        return markerId;
    }

    public void setMarkerId(String markerId) {
        this.markerId = markerId;
    }

    public List<GeoPointData> getGeoPoints() {
        return geoPoints;
    }

    public void setGeoPoints(List<GeoPointData> geoPoints) {
        this.geoPoints = geoPoints;
    }

    @Override
    public String toString() {
        return "GeoPathData{" +
                "geoPoints=" + geoPoints +
                ", markerId=" + markerId +
                '}';
    }
}
