package net.molchat.mototrack.db;

/**
 * @author Valentyn Markovych, Gorilla
 */
public class DbManager {

    protected final static DbManager instance = new DbManager();
    protected DbService dbService;

    protected DbManager() {
        dbService = new InMemoryDbService();
    }

    public static DbManager getInstance() {

        return instance;
    }

    public DbService getDbService() {

        return dbService;
    }
}
