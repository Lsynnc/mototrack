package net.molchat.mototrack.l20n;

import java.util.Map;

/**
 * @author Valentyn Markovych, Gorilla
 */
public class LocalizedString {

    public static String defaultLocale = "en_EN";

    protected Map<String, String> values;

    public void setValue(String text) {

        setValue(defaultLocale, text);
    }


    public void setValue(String locale, String text) {

        values.put(locale, text);
    }
}
