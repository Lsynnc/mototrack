package net.molchat.mototrack.api;

import net.molchat.mototrack.db.DbManager;
import net.molchat.mototrack.db.DbService;
import net.molchat.mototrack.db.entity.GeoPointData;

import java.util.List;

/**
 * @author Valentyn Markovych, Gorilla
 */
public class PointServiceImpl implements PointService {

    private DbService dbService = DbManager.getInstance().getDbService();


    @Override
    public GeoPointData getLastUserLocation(long userId) {

        return null;
    }

    @Override
    public List<GeoPointData> getPointsForUser(long userId, long timePeriodMillis) {
        return null;
    }

    @Override
    public void reportPoint(GeoPointData geoPointReportData) {

    }
}
