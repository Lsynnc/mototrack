package net.molchat.mototrack.api;

import net.molchat.mototrack.db.entity.GeoPointData;

import java.util.List;

/**
 * @author Valentyn Markovych, Gorilla
 */
public interface PointService {

    public GeoPointData getLastUserLocation(long userId);

    public List<GeoPointData> getPointsForUser(long userId, long timePeriodMillis);

    public void reportPoint(GeoPointData geoPointReportData);
}
