package net.molchat.mototrack.settings;

/**
 * @author Valentyn Markovych, Gorilla
 */
public class Settings {

    private final static Settings instance = new Settings();
    private int defaultMaxMinutes = 10080; // 10080 - one week

    private Settings() {

    }

    public static Settings getInstance() {
        return instance;
    }


    private int defaultPointsNum = 50;
    private int maxPointsNum = 10000;

    protected String defaultDateTimeParamFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'";

    public int getDefaultPointsNum() {

        return defaultPointsNum;
    }

    public void setDefaultPointsNum(int defaultPointsNum) {

        this.defaultPointsNum = defaultPointsNum;
    }

    public int getMaxPointsNum() {

        return maxPointsNum;
    }

    public void setMaxPointsNum(int maxPointsNum) {

        this.maxPointsNum = maxPointsNum;
    }

    public int getDefaultMaxMinutes() {

        return defaultMaxMinutes;
    }

    public void setDefaultMaxMinutes(int defaultMaxMinutes) {
        this.defaultMaxMinutes = defaultMaxMinutes;
    }

    public String getDefaultDateTimeParamFormat() {
        return defaultDateTimeParamFormat;
    }

    public void setDefaultDateTimeParamFormat(String defaultDateTimeParamFormat) {
        this.defaultDateTimeParamFormat = defaultDateTimeParamFormat;
    }
}
