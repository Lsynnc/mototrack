package net.molchat.mototrack.model;

import net.molchat.mototrack.l20n.LocalizedString;

/**
 * @author Valentyn Markovych, Gorilla
 */
public interface AbstractPageModel {

    public LocalizedString getTitle();
}
