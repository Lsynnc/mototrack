/**
 * Created by Valentyn on 02.09.2015.
 */

// Construct TMap object
var TMap = {};
TMap.settings = {
    apiBaseUrl: "/api/",
    apiPathUrl: "/api/path?c=get&id=123&maxPoints=6000&maxMinutes=120",
    pathSegments: [[10, "#bd0026"], [30, "#f03b20"], [120, "#fd8d3c"], [9999, "fecc5c"]], // [minutes, color]
//    pathSegments: [[1, "#0000ff"], [2, "#00ff00"], [3, "#555555"]], // [minutes, color], test data
    updateTime: 10000
};

TMap.current = {
    point: null,
    path: {}
}


var map = L.map('map').setView([50.48, 30.55], 13);

L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    id: 'mapbox.streets'
}).addTo(map);


/**
 *
 * @param method HTTP request method, like "GET", "POST" etc.
 * @param url URL to call to
 */
function ajaxCall(/*String*/ method, /*String*/ url, /*function*/ callback, /*String*/ data) {

    var xmlhttp;
    if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    }
    else {// code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            callback(xmlhttp);
        }
    }
    xmlhttp.open(method, url, true);
    xmlhttp.send(data);
}


function renewMarker(/*int*/ markerId, /*geoPoints[]*/ geoPoints) {

    if (geoPoints.length > 0) {

        var lastPoint = geoPoints[geoPoints.length - 1];
        var currentTime = new Date().getTime();
        console.log("Renewing marker " + markerId + " to [" + lastPoint.x + ", " + lastPoint.y + "]");
        if (TMap.current.point == null) {
            TMap.current.point = L.marker([lastPoint.x, lastPoint.y]).addTo(map);
            scrollMap(lastPoint);
        }
        TMap.current.point.setLatLng([lastPoint.x, lastPoint.y]);
        TMap.current.point.bindPopup("<b>Who: " + markerId + "</b><br />" + (Math.floor((currentTime - lastPoint.timestamp)
            / 1000 / 60)) + " min ago");
    }
}

/**
 * TODO Should scroll map as well, if:
 * - the maps shows only one marker
 * - time of inactivity is more, then set up threshold.
 *
 * @param geoPoint - current point of the main (tracked) marker
 */
function scrollMap(/*geoPoint*/ geoPoint) {

    map.setView([geoPoint.x, geoPoint.y]);
}


function gotPointsCallback(xmlhttpResponse) {

    var respObj = JSON.parse(xmlhttpResponse.responseText)
    console.log("Response object: ");
    console.log(respObj);
    var geoPoints = respObj.geoPoints;
    if (typeof geoPoints !== 'undefined') {
        var markerId = respObj.markerId;
        renewMarker(markerId, geoPoints);
        renewPath(markerId, geoPoints);
        //scrollMap(lastPoint.x, lastPoint.y);
    }
}


function renewPath(/*int*/markerId, geoPointsArray) {

    var geoJSON = geoPointsToJSONPath(geoPointsArray);
    var geoJsonLayer = L.geoJson(geoJSON, {
        style: function (feature) {
            return {
                "color": getSegmentColor(feature.properties.timeStamp),
                "opacity": getSegmentOpacity(feature.properties.timeStamp),
                "weight": 5
            }
        },
        onEachFeature: onEachGeoJsonFeature
    });
    map.removeLayer(TMap.current.path);
    TMap.current.path = geoJsonLayer;
    TMap.current.path.addTo(map);
}


function geoPointsToJSONPath(geoPointsArray) {

    var geoJson = [];
    for (var i = 1; i < geoPointsArray.length; i++) {
        addSegment(geoJson, geoPointsArray[i - 1], geoPointsArray[i]);
    }

    console.log("New geoJson:");
    console.log(geoJson);
    return geoJson;
}

function addSegment(geoJson, geoPointPrevious, geoPointNext) {

    var segment = {};
    segment.type = "Feature";
    segment.properties = {"timeStamp": geoPointNext.timestamp};
    segment.geometry = {
        "type": "LineString"
    };
    segment.geometry.coordinates = [[geoPointPrevious.y, geoPointPrevious.x], [geoPointNext.y, geoPointNext.x]];
    geoJson.push(segment);
}


function getSegmentColor(timeStamp) {
    var currentTimeStamp = new Date().getTime();
    var timeDiffMinutes = (currentTimeStamp - timeStamp) / 1000 / 60;

    return timeDiffMinutes < TMap.settings.pathSegments[0][0] ? '#bd0026' :
        timeDiffMinutes < TMap.settings.pathSegments[1][0] ? '#f03b20' :
            timeDiffMinutes < TMap.settings.pathSegments[2][0] ? '#fd8d3c' :
                timeDiffMinutes < TMap.settings.pathSegments[3][0] ? '#fecc5c' :
                    '#ffffb2';
};

function getSegmentOpacity() {

    return 1;
}


function onEachGeoJsonFeature(feature, layer) {

    var currentTimeStamp = new Date().getTime();
    var timeDiffMinutes = Math.floor((currentTimeStamp - feature.properties.timeStamp) / 1000 / 60);

    layer.bindPopup("" + timeDiffMinutes + " min ago");
}


function renewPoints() {

    ajaxCall("GET", TMap.settings.apiPathUrl, gotPointsCallback, null);
}

function onMapClick(e) {
}

// Set the marker first time
renewPoints();
var pointRenewTimer = setInterval(renewPoints, TMap.settings.updateTime);

var popup = L.popup();
map.on('click', onMapClick);

